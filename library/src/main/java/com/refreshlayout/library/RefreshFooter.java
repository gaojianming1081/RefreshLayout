package com.refreshlayout.library;

import com.refreshlayout.library.constants.LoadMoreState;
import com.refreshlayout.library.listener.AbstractRefreshFooter;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

/**
 * 底部组件
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public class RefreshFooter extends DirectionalLayout {
    private AbstractRefreshFooter mAbstractRefreshFooter;

    private LoadMoreState mRefreshState = LoadMoreState.STATE_NORMAL;

    private int mLoadMoreComponentHeight;

    private int mLastProgress;

    public RefreshFooter(Context context) {
        super(context);
        initView(context);
    }

    public RefreshFooter(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context);
    }

    public RefreshFooter(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        mContext = context;
        setHeight(0);
    }

    /**
     * 新增底部加载更多组件
     *
     * @param refreshFooter 底部加载更多组件
     */
    public void addFooterComponent(AbstractRefreshFooter refreshFooter) {
        this.mAbstractRefreshFooter = refreshFooter;
        Component mContentView = refreshFooter.getFooterComponent();
        mContentView.setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                mLoadMoreComponentHeight = component.getHeight();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
            }
        });
        LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        if(mContentView.getComponentParent() == null) {
            addComponent(mContentView, layoutConfig);
        }else {
            throw new IllegalStateException("Component can only have one parent, but it has been added more than twice");
        }
        setAlignment(LayoutAlignment.TOP);
    }

    public void startLoadMore() {
        if(mAbstractRefreshFooter != null) {
            mAbstractRefreshFooter.onPullLoadMoreStateChange(LoadMoreState.STATE_LOADING);
        }
    }

    public void stopLoadMore() {
        if(mAbstractRefreshFooter != null) {
            mAbstractRefreshFooter.onPullLoadMoreStateChange(LoadMoreState.STATE_NORMAL);
        }
    }

    public void setVisibleHeight(float height) {
        setTranslationY(height);
    }

    public float getVisibleHeight() {
        return getTranslationY();
    }

    public float getRefreshFooterHeight() {
        return mLoadMoreComponentHeight;
    }

    public boolean isCanLoadMore() {
        return Math.abs(getVisibleHeight()) > Math.abs(mLoadMoreComponentHeight);
    }

    /**
     * 更新加载更多上拉率，最大值为100
     */
    public void updateLoadMoreRateProgress() {
        int progress = Math.abs((int)(getVisibleHeight() * 100f / getRefreshFooterHeight()));
        //判断刷新状态
        LoadMoreState mCurState = null;
        if (progress < 100 && mLastProgress >= 100) {
            mCurState = LoadMoreState.STATE_NORMAL;
        }
        if (progress >= 100 && mLastProgress < 100) {
            mCurState = LoadMoreState.STATE_READY_LOADMORE;
        }
        // 调用相关回调方法
        if(mAbstractRefreshFooter != null) {
            mAbstractRefreshFooter.onPullLoadMoreRate(Math.min(100, progress));
            if(mCurState != null && mCurState != mRefreshState) {
                mRefreshState = mCurState;
                mAbstractRefreshFooter.onPullLoadMoreStateChange(mCurState);
            }
        }

        mLastProgress = progress;
    }

}