package com.refreshlayout.library.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 获取自定义属性工具类,如果没有配置这个自定义属性则使用默认值。
 *
 * @since 2021-08-01
 */
public class AttrUtils {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x78002, AttrUtils.class.getSimpleName());

    private AttrUtils() {
    }

    /**
     * 获取Float资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static float getFloatValueByAttr(AttrSet attrSet, String filedName, float defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getFloatValue();
        }
        return defaultValue;
    }

    /**
     * 获取Int资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static int getIntValueByAttr(AttrSet attrSet, String filedName, int defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getIntegerValue();
        }
        return defaultValue;
    }

    /**
     * 获取Color资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static Color getColorValueByAttr(AttrSet attrSet, String filedName, Color defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getColorValue();
        }
        HiLog.info(LABEL, "getColorValueByAttr return defaultValue");
        return defaultValue;
    }

    /**
     * 获取Dimension资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static int getDimensionValueByAttr(AttrSet attrSet, String filedName, int defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getDimensionValue();
        }
        return defaultValue;
    }

    /**
     * 获取String资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static String getStringValueByAttr(AttrSet attrSet, String filedName, String defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getStringValue();
        }
        return defaultValue;
    }

    /**
     * 获取Boolean资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static boolean getBooleanValueByAttr(AttrSet attrSet, String filedName, boolean defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getBoolValue();
        }
        return defaultValue;
    }

    /**
     * 获取Element元素值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 元素值
     */
    public static Element getElementValueByAttr(AttrSet attrSet, String filedName, Element defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getElement();
        }
        return defaultValue;
    }

    private static Attr get(AttrSet attrSet, String filedName) {
        if (attrSet != null && attrSet.getAttr(filedName).isPresent()) {
            return attrSet.getAttr(filedName).get();
        }
        return null;
    }
}
