package com.refreshlayout.library.listener;

import com.refreshlayout.library.constants.LoadMoreState;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.utils.PlainArray;

/**
 * 自定义底部组件基类
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public abstract class AbstractRefreshFooter {
    private final PlainArray<Component> mComponentMap = new PlainArray<>();
    private Component mFooterComponent;

    public AbstractRefreshFooter(Context context) {
        init(context);
    }

    private void init(Context context) {
        mFooterComponent = LayoutScatter.getInstance(context).parse(getFooterLayoutId(), null, false);
    }

    /**
     * 通过id获取对应组件，此处通过Map缓存方式存储Component，避免重复获取
     *
     * @param layoutId 组件id
     * @return 获取到的组件
     */
    public Component getComponentById(int layoutId) {
        if(mFooterComponent == null) {
            return null;
        }
        // 先从缓存取，没有再从布局load
        Component component = mComponentMap.get(layoutId, null);
        if(component == null) {
            component = mFooterComponent.findComponentById(layoutId);
            if(component != null) {
                mComponentMap.put(layoutId, component);
            }
        }
        return component;
    }

    public Component getFooterComponent() {
        return mFooterComponent;
    }

    /**
     * 上拉加载更多的上拉率
     *
     * @param progress 上拉率 0-100
     */
    public abstract void onPullLoadMoreRate(int progress);

    /**
     * 获取底部布局ID
     *
     * @return 底部布局ID
     */
    public abstract int getFooterLayoutId();

    /**
     * 上拉加载更多状态
     *
     * @param loadMoreState 当前上拉加载更多状态
     */
    public abstract void onPullLoadMoreStateChange(LoadMoreState loadMoreState);
}
