package com.refreshlayout.library.listener;

import com.refreshlayout.library.constants.RefreshState;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.utils.PlainArray;

/**
 * 自定义头部组件基类
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public abstract class AbstractRefreshHeader {
    private final PlainArray<Component> mComponentMap = new PlainArray<>();

    private Component mHeaderComponent;

    public AbstractRefreshHeader(Context context) {
        init(context);
    }

    private void init(Context context) {
        mHeaderComponent = LayoutScatter.getInstance(context).parse(getHeaderLayoutId(), null, false);
    }

    /**
     * 通过id获取对应组件，此处通过Map缓存方式存储Component，避免重复获取
     *
     * @param layoutId 组件id
     * @return 获取到的组件
     */
    public Component getComponentById(int layoutId) {
        if(mHeaderComponent == null) {
            return null;
        }
        // 先从缓存取，没有再从布局load
        Component component = mComponentMap.get(layoutId, null);
        if(component == null) {
            component = mHeaderComponent.findComponentById(layoutId);
            if(component != null) {
                mComponentMap.put(layoutId, component);
            }
        }
        return component;
    }

    public Component getHeaderComponent() {
        return mHeaderComponent;
    }

    /**
     * 下拉刷新下拉率
     *
     * @param progress 下拉率 0-100
     */
    public abstract void onPullRefreshRate(int progress);

    /**
     * 获取头部布局ID
     *
     * @return 头部布局ID
     */
    public abstract int getHeaderLayoutId();

    /**
     * 下拉刷新状态
     *
     * @param refreshState 当前刷新的状态
     */
    public abstract void onPullRefreshStateChange(RefreshState refreshState);

}
