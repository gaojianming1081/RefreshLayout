package com.refreshlayout.library.listener;

import ohos.agp.animation.Animator;

/**
 * 动画状态监听
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public class AnimationStateListener implements Animator.StateChangedListener {
    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {

    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }
}
