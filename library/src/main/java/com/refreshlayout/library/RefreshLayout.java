package com.refreshlayout.library;

import com.refreshlayout.library.listener.AbstractRefreshFooter;
import com.refreshlayout.library.listener.AbstractRefreshHeader;
import com.refreshlayout.library.listener.AnimationStateListener;
import com.refreshlayout.library.utils.AttrUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 刷新主组件
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public class RefreshLayout extends DirectionalLayout implements Component.TouchEventListener {
    private static final int MAX_DURATION = 350;
    private static final float OFFSET_RADIO = 1.8f;

    private RefreshHeader mRefreshHeader;
    private boolean mEnablePullRefresh = true;
    private boolean mPullRefreshing = false;

    private RefreshFooter mRefreshFooter;
    private boolean mEnablePullLoad;
    private boolean mPullLoading;

    private float mLastY;

    private Component childComponent;

    private IRefreshListener mRefreshListener;

    private EventHandler mEventHandler;

    private int refreshLayoutId;

    public RefreshLayout(Context context) {
        super(context);
        initWithContext(context, null);
    }

    public RefreshLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initWithContext(context, attrSet);
    }

    public RefreshLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initWithContext(context, attrSet);
    }

    private void initWithContext(Context context, AttrSet attrSet) {
        setTouchEventListener(this);
        setOrientation(Component.VERTICAL);
        mRefreshHeader = new RefreshHeader(context);
        addComponent(mRefreshHeader);

        mRefreshFooter = new RefreshFooter(context);

        initAttrSet(attrSet);
    }

    private void initAttrSet(AttrSet attrSet) {
        if(attrSet != null) {
            if(attrSet.getAttr("bindRefreshLayoutId").isPresent()){
                refreshLayoutId = AttrUtils.getIntValueByAttr(attrSet, "bindRefreshLayoutId", -1);
            }
        }
    }

    /**
     * 添加头部刷新组件
     *
     * @param refreshHeader 自定义的头部刷新组件
     */
    public void addHeaderComponent(AbstractRefreshHeader refreshHeader) {
        mRefreshHeader.addHeaderComponent(refreshHeader);
    }

    /**
     * 添加底部加载更多组件
     *
     * @param refreshFooter 自定义底部加载更多组件
     */
    public void addFooterComponent(AbstractRefreshFooter refreshFooter) {
        mRefreshFooter.addFooterComponent(refreshFooter);
        LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        addComponent(mRefreshFooter, layoutConfig);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if(childComponent == null) {
            if(refreshLayoutId > 0) {
                childComponent = findComponentById(refreshLayoutId);
            }else {
                childComponent = getComponentAt(1);
            }
        }
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                childComponent.setEnabled(true);
                mLastY = touchEvent.getPointerScreenPosition(0).getY();
                return true;
            case TouchEvent.POINT_MOVE:
                float mCurY = touchEvent.getPointerScreenPosition(0).getY();
                float offsetY = mCurY - mLastY;
                if(!mPullRefreshing && !mPullLoading) {
                    if(offsetY > 0) {
                        // 下拉
                        if(!childComponent.canScroll(DRAG_DOWN) && mRefreshFooter.getVisibleHeight() >= 0) {
                            updateHeaderHeight(childComponent, offsetY/OFFSET_RADIO);
                        }else {
                            if(Math.abs(mRefreshFooter.getVisibleHeight()) > 0 && !childComponent.canScroll(DRAG_UP)) {
                                updateFooterHeight(childComponent, offsetY/OFFSET_RADIO);
                            }else {
                                childComponent.setEnabled(true);
                            }
                        }
                    }else {
                        // 上拉
                        if(mRefreshHeader.getVisibleHeight() > 0 && !childComponent.canScroll(DRAG_DOWN)) {
                            updateHeaderHeight(childComponent, offsetY/OFFSET_RADIO);
                        }else {
                            if(childComponent.canScroll(DRAG_UP)) {
                                childComponent.setEnabled(true);
                            }else {
                                updateFooterHeight(childComponent, offsetY/OFFSET_RADIO);
                            }
                        }
                    }
                }
                mLastY = mCurY;
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                // 手指释放后的逻辑处理
                if(!mPullRefreshing && !mPullLoading) {
                    if(mRefreshHeader.isCanRefresh()) {
                        startRefresh();
                    }else {
                        resetHeaderAnimation();
                    }

                    if(mRefreshFooter.isCanLoadMore()) {
                        startLoadMore();
                    }else {
                        resetFooterAnimation();
                    }
                }
                break;
            case TouchEvent.CANCEL:
                break;
        }
        return false;
    }

    /**
     * 更新头部刷新布局的高度
     *
     * @param childComponent 一般为包裹的滚动组件
     * @param delta 每次更新的高度值
     */
    private void updateHeaderHeight(Component childComponent, float delta) {
        if(mEnablePullRefresh && !mPullRefreshing) {
            childComponent.setEnabled(false);
            mRefreshHeader.setVisibleHeight((int) delta + mRefreshHeader.getVisibleHeight());

            mRefreshHeader.updateRefreshRateProgress();
        }
    }

    /**
     * 更新底部加载更多布局高度
     *
     * @param childComponent 一般为包裹的滚动组件
     * @param delta 每次更新的高度值
     */
    private void updateFooterHeight(Component childComponent, float delta) {
        if(mEnablePullLoad && !mPullLoading) {
            childComponent.setEnabled(false);
            childComponent.setTranslationY(childComponent.getTranslationY() + delta);
            mRefreshFooter.setVisibleHeight(childComponent.getTranslationY());

            mRefreshFooter.updateLoadMoreRateProgress();
        }
    }

    /**
     * 头部刷新布局重置后的动画交互
     */
    private void resetHeaderAnimation() {
        int height = mRefreshHeader.getVisibleHeight();
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, height));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            mRefreshHeader.setVisibleHeight((int) (height * (1 - v)));
            mRefreshHeader.updateRefreshRateProgress();
        });
        animatorValue.start();
    }

    /**
     * 底部加载更多布局重置后的动画交互
     */
    private void resetFooterAnimation() {
        float height = mRefreshFooter.getVisibleHeight();
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, (int)height));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            float heightValue = height * (1 - v);
            childComponent.setTranslationY(heightValue);
            mRefreshFooter.setVisibleHeight(heightValue);
            mRefreshFooter.updateLoadMoreRateProgress();
        });
        animatorValue.start();
    }

    /**
     * 开始刷新
     */
    private void startRefresh() {
        mPullRefreshing = true;
        mRefreshHeader.startRefresh();
        int mSrcHeight = mRefreshHeader.getRefreshHeaderHeight();
        int mDesHeight = mRefreshHeader.getVisibleHeight();
        int deltaHeight = mDesHeight - mSrcHeight;
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, deltaHeight));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            mRefreshHeader.setVisibleHeight(mSrcHeight + (int) (deltaHeight * (1 - v)));
            if(v == 1.0f) {
                if(mRefreshListener != null) {
                    mRefreshListener.onRefresh();
                }
            }
        });
        animatorValue.start();
    }

    /**
     * 开始加载更多
     */
    private void startLoadMore() {
        mPullLoading = true;
        mRefreshFooter.startLoadMore();
        float mSrcHeight = mRefreshFooter.getRefreshFooterHeight();
        float mDesHeight = Math.abs(mRefreshFooter.getVisibleHeight());
        float deltaHeight = mDesHeight - mSrcHeight;
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, (int)deltaHeight));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            float heightValue = -mSrcHeight - (deltaHeight * (1 - v));
            childComponent.setTranslationY(heightValue);
            mRefreshFooter.setVisibleHeight(heightValue);
            if(v == 1.0f) {
                if(mRefreshListener != null) {
                    mRefreshListener.onLoadMore();
                }
            }
        });
        animatorValue.start();
    }

    /**
     * 获取统一的EventHandler
     *
     * @return EventHandler
     */
    private EventHandler getEventHandler() {
        if(mEventHandler == null) {
            mEventHandler = new EventHandler(EventRunner.getMainEventRunner());
        }
        return mEventHandler;
    }

    /**
     * 延迟停止刷新
     *
     * @param delayTimeMs 延迟时间ms
     */
    public void stopRefresh(long delayTimeMs) {
        getEventHandler().postTask(this::stopRefresh, delayTimeMs);
    }

    /**
     * 停止刷新
     */
    public void stopRefresh() {
        if(mPullRefreshing) {
            mPullRefreshing = false;
        }
        mRefreshHeader.stopRefresh();
        int canScrollMaxValue = childComponent.getScrollValue(AXIS_Y);
        int height = mRefreshHeader.getVisibleHeight();
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, height));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            int heightValue = (int) (height * (1 - v));
            mRefreshHeader.setVisibleHeight(heightValue);
            mRefreshHeader.updateRefreshRateProgress();
        });
        animatorValue.setStateChangedListener(new AnimationStateListener(){
            @Override
            public void onEnd(Animator animator) {
                childComponent.scrollTo(0, canScrollMaxValue);
            }
        });
        animatorValue.start();
    }

    /**
     * 延迟停止加载更多
     *
     * @param delayTimeMs 延迟时间ms
     */
    public void stopLoadMore(long delayTimeMs) {
        getEventHandler().postTask(this::stopLoadMore, delayTimeMs);
    }

    /**
     * 停止加载更多
     */
    public void stopLoadMore() {
        if(mPullLoading) {
            mPullLoading = false;
        }
        mRefreshFooter.stopLoadMore();
        float height = mRefreshFooter.getVisibleHeight();
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, (int)height));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            float heightValue = height * (1 - v);
            childComponent.setTranslationY(heightValue);
            mRefreshFooter.setVisibleHeight(heightValue);
            mRefreshFooter.updateLoadMoreRateProgress();
        });
        animatorValue.start();
    }

    /**
     * 延迟自动刷新
     *
     * @param delayTimeMs 延迟时间ms
     */
    public void autoRefresh(long delayTimeMs) {
        getEventHandler().postTask(this::autoRefresh, delayTimeMs);
    }

    /**
     * 自动刷新
     */
    public void autoRefresh() {
        int height = mRefreshHeader.getRefreshHeaderHeight();
        if(height == 0) {
            throw new IllegalArgumentException("Call autoRefresh after addHeaderComponent");
        }
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.DECELERATE);
        animatorValue.setDuration(Math.min(MAX_DURATION, height));
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            mRefreshHeader.setVisibleHeight((int) (height * v));
            mRefreshHeader.updateRefreshRateProgress();
        });
        animatorValue.setStateChangedListener(new AnimationStateListener(){
            @Override
            public void onStart(Animator animator) {
                mPullRefreshing = true;
            }

            @Override
            public void onEnd(Animator animator) {
                if(mRefreshListener != null) {
                    mRefreshListener.onRefresh();
                }
            }
        });
        animatorValue.start();
    }

    /**
     * 设置是否支持刷新功能
     *
     * @param enableRefresh 能否刷新
     */
    public void setRefreshEnable(boolean enableRefresh) {
        this.mEnablePullRefresh = enableRefresh;
    }

    /**
     * 设置是否支持加载更多功能
     *
     * @param enableLoadMore 能否加载更多
     */
    public void setLoadMoreEnable(boolean enableLoadMore) {
        this.mEnablePullLoad = enableLoadMore;
    }

    /**
     * 设置刷新和加载更多监听
     *
     * @param mRefreshListener 监听
     */
    public void setRefreshListener(IRefreshListener mRefreshListener) {
        this.mRefreshListener = mRefreshListener;
    }

    public interface IRefreshListener {
        void onRefresh();
        void onLoadMore();
    }
}
