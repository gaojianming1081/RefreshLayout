package com.refreshlayout.library;

import com.refreshlayout.library.constants.RefreshState;
import com.refreshlayout.library.listener.AbstractRefreshHeader;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 刷新头部组件
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public class RefreshHeader extends DirectionalLayout {
    private Context mContext;

    private Component mContainer;

    private AbstractRefreshHeader mAbstractRefreshHeader;

    private RefreshState mRefreshState = RefreshState.STATE_NORMAL;

    private int mRefreshHeaderHeight;

    private int mLastProgress;

    public RefreshHeader(Context context) {
        super(context);
        initView(context);
    }

    public RefreshHeader(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context);
    }

    public RefreshHeader(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        this.mContext = context;
        setHeight(0);
    }

    /**
     * 新增头部刷新组件
     *
     * @param refreshHeader 头部刷新组件
     */
    public void addHeaderComponent(AbstractRefreshHeader refreshHeader) {
        this.mAbstractRefreshHeader = refreshHeader;
        Component mHeaderContainer = refreshHeader.getHeaderComponent();
        mHeaderContainer.setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                mRefreshHeaderHeight = component.getHeight();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
            }
        });
        // 初始情况，设置下拉刷新component高度为MATCH_CONTENT
        int screenWidth = DisplayManager.getInstance().getDefaultDisplay(mContext).get().getAttributes().width;
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(screenWidth, LayoutConfig.MATCH_CONTENT);
        mContainer = mHeaderContainer;
        layoutConfig.setMarginTop(-mRefreshHeaderHeight);
        if(mContainer.getComponentParent() == null) {
            addComponent(mContainer, layoutConfig);
        }else {
            throw new IllegalStateException("Component can only have one parent, but it has been added more than twice");
        }
        setAlignment(LayoutAlignment.BOTTOM);
    }

    /**
     * 设置刷新组件头部可见高度
     *
     * @param height 可见高度
     */
    public void setVisibleHeight(int height) {
        if (height < 0)
            height = 0;
        DirectionalLayout.LayoutConfig layoutConfig = (DirectionalLayout.LayoutConfig) getLayoutConfig();
        layoutConfig.height = height;
        setLayoutConfig(layoutConfig);
    }

    public int getVisibleHeight() {
        return getLayoutConfig().height;
    }

    public int getRefreshHeaderHeight() {
        return mRefreshHeaderHeight;
    }

    public boolean isCanRefresh() {
        return getRefreshHeaderHeight() < getVisibleHeight();
    }

    public boolean isAddHeaderComponent() {
        return mContainer != null;
    }

    public void startRefresh() {
        if(mAbstractRefreshHeader != null) {
            mAbstractRefreshHeader.onPullRefreshStateChange(RefreshState.STATE_REFRESHING);
        }
    }

    public void stopRefresh() {
        if(mAbstractRefreshHeader != null) {
            mAbstractRefreshHeader.onPullRefreshStateChange(RefreshState.STATE_NORMAL);
        }
    }

    /**
     * 更新下拉刷新下拉率，最大下拉率为100
     */
    public void updateRefreshRateProgress() {
        int progress = (int)(getVisibleHeight() * 100f / getRefreshHeaderHeight());
        //判断刷新状态
        RefreshState mCurState = null;
        if (progress < 100 && mLastProgress >= 100) {
            mCurState = RefreshState.STATE_NORMAL;
        }
        if (progress >= 100 && mLastProgress < 100) {
            mCurState = RefreshState.STATE_READY_REFRESH;
        }
        // 调用相关回调方法
        if(mAbstractRefreshHeader != null) {
            mAbstractRefreshHeader.onPullRefreshRate(Math.min(100, progress));
            if(mCurState != null && mCurState != mRefreshState) {
                mRefreshState = mCurState;
                mAbstractRefreshHeader.onPullRefreshStateChange(mCurState);
            }
        }

        mLastProgress = progress;
    }
}

