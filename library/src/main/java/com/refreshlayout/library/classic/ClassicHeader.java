package com.refreshlayout.library.classic;

import com.refreshlayout.library.ResourceTable;
import com.refreshlayout.library.constants.RefreshState;
import com.refreshlayout.library.listener.AbstractRefreshHeader;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * 经典头部刷新样式
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public class ClassicHeader extends AbstractRefreshHeader {
    private AnimatorValue animatorValue;
    private AnimatorValue progressAnimValue;

    public ClassicHeader(Context context) {
        super(context);
    }

    @Override
    public void onPullRefreshRate(int progress) {

    }

    @Override
    public int getHeaderLayoutId() {
        return ResourceTable.Layout_classic_header_layout;
    }

    @Override
    public void onPullRefreshStateChange(RefreshState refreshState) {
        Text text = (Text) getComponentById(ResourceTable.Id_header_hint_textview);
        Image arrowImage = (Image) getComponentById(ResourceTable.Id_header_arrow);
        Image refreshImage = (Image) getComponentById(ResourceTable.Id_header_refresh);
        switch (refreshState) {
            case STATE_NORMAL:
                text.setText(ResourceTable.String_header_hint_normal);
                arrowImage.setVisibility(Component.VISIBLE);
                refreshImage.setVisibility(Component.INVISIBLE);
                startArrowAnim(arrowImage, true);
                if(progressAnimValue != null) {
                    progressAnimValue.stop();
                }
                break;
            case STATE_READY_REFRESH:
                text.setText(ResourceTable.String_header_hint_ready);
                arrowImage.setVisibility(Component.VISIBLE);
                refreshImage.setVisibility(Component.INVISIBLE);
                startArrowAnim(arrowImage, false);
                if(progressAnimValue != null) {
                    progressAnimValue.stop();
                }
                break;
            case STATE_REFRESHING:
                text.setText(ResourceTable.String_header_hint_refreshing);
                arrowImage.setVisibility(Component.INVISIBLE);
                refreshImage.setVisibility(Component.VISIBLE);
                startRefreshAnim();
                break;
            default:
                break;
        }
    }

    private void startArrowAnim(Image arrowImage, boolean isUpAnim) {
        arrowImage.setRotation(0);
        if (animatorValue != null && animatorValue.isRunning()) {
            animatorValue.stop();
        }
        if (animatorValue == null) {
            animatorValue = new AnimatorValue();
            animatorValue.setDuration(200);
            animatorValue.setCurveType(Animator.CurveType.LINEAR);
        }
        animatorValue.setValueUpdateListener((animatorValue, v) -> arrowImage.setRotation(isUpAnim ? ((1 - v) * 180) : (v * 180)));
        animatorValue.start();
    }

    private void startRefreshAnim() {
        Image refreshImage = (Image) getComponentById(ResourceTable.Id_header_refresh);
        if(progressAnimValue == null) {
            progressAnimValue = new AnimatorValue();
            progressAnimValue.setDuration(500);
            progressAnimValue.setCurveType(Animator.CurveType.LINEAR);
            progressAnimValue.setLoopedCount(Integer.MAX_VALUE);
        }
        progressAnimValue.setValueUpdateListener((animatorValue1, v) -> refreshImage.setRotation(v*360));
        progressAnimValue.start();
    }
}
