package com.refreshlayout.library.constants;

/**
 * 加载更多状态
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public enum LoadMoreState {
    // 正常状态
    STATE_NORMAL,
    // 准备加载更多状态
    STATE_READY_LOADMORE,
    // 正在Loading
    STATE_LOADING
}
