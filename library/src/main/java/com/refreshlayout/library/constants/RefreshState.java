package com.refreshlayout.library.constants;

/**
 * 刷新状态
 *
 * @author gaojianming
 * @since 2021-08-01
 */
public enum RefreshState {
    // 正常状态
    STATE_NORMAL,
    // 准备刷新状态
    STATE_READY_REFRESH,
    // 正在刷新状态
    STATE_REFRESHING
}
