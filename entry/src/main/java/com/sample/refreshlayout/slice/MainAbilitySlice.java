package com.sample.refreshlayout.slice;

import com.refreshlayout.library.RefreshLayout;
import com.refreshlayout.library.classic.ClassicFooter;
import com.refreshlayout.library.classic.ClassicHeader;
import com.sample.refreshlayout.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        RefreshLayout refreshLayout = (RefreshLayout) findComponentById(ResourceTable.Id_refreshLayout);
        refreshLayout.setLoadMoreEnable(true);
        refreshLayout.setRefreshListener(new RefreshLayout.IRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.stopRefresh(3000);
            }

            @Override
            public void onLoadMore() {
                refreshLayout.stopLoadMore(3000);
            }
        });

        refreshLayout.addHeaderComponent(new ClassicHeader(this));
        refreshLayout.addFooterComponent(new ClassicFooter(this));

        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        listContainer.setItemProvider(new MyProvider(this));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private static class MyProvider extends BaseItemProvider{
        private final Context context;

        private MyProvider(Context context){
            this.context = context;
        }

        @Override
        public int getCount() {
            return 30;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Component getComponent(int i, Component convertComponent, ComponentContainer componentContainer) {
            final Component cpt;
            if (convertComponent == null) {
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_list_item, null, false);
            } else {
                cpt = convertComponent;
            }
            Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);
            text.setText("Hello World:" + i);
            return cpt;
        }
    }
}
