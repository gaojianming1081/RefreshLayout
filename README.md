## RefreshLayout

一款高效、简洁、好用的下拉刷新以及上拉加载更多的组件，可随意自定义自己想要的刷新和上拉加载更多的效果

### 如何集成
1.在项目根目录下的build.gradle文件中，
```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:RefreshLayout:1.0.0')
    ......  
 }
```

### 如何使用
1. 在xml布局文件中的使用方法
```xml
<!--第一种使用方式-->
<com.refreshlayout.library.RefreshLayout
        ohos:id="$+id:refreshLayout"
        ohos:height="match_parent"
        ohos:width="match_parent"
        >

        <ListContainer
            ohos:id="$+id:listContainer"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:long_click_enabled="false"
            ohos:background_element="#FFFFFFFF"
            />

</com.refreshlayout.library.RefreshLayout>

<!--第二种使用方式-->
<!--当滚动组件为一个嵌套的比较深的组件时，需要通过bindRefreshLayoutId属性指定绑定关系，否则无法监听是哪个组件需要下拉刷新，因为默认是第一个子组件-->
<com.refreshlayout.library.RefreshLayout
      ohos:id="$+id:refreshLayout"
      ohos:height="match_parent"
      ohos:width="match_parent"
      app:bindRefreshLayoutId="$id:listContainer"
      >

      <DependentLayout
          ohos:height="match_parent"
          ohos:width="match_parent">

          <ListContainer
              ohos:id="$+id:listContainer"
              ohos:height="match_parent"
              ohos:width="match_parent"
              ohos:long_click_enabled="false"
              ohos:background_element="#FFFFFFFF"
              />

      </DependentLayout>

</com.refreshlayout.library.RefreshLayout>

```

2. 在java代码中的用法
```java
        RefreshLayout refreshLayout = (RefreshLayout) findComponentById(ResourceTable.Id_refreshLayout);
        refreshLayout.setLoadMoreEnable(true);
        refreshLayout.setRefreshListener(new RefreshLayout.IRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.stopRefresh(3000);
            }

            @Override
            public void onLoadMore() {
                refreshLayout.stopLoadMore(3000);
            }
        });

        // ClassicHeader和ClassicFooter为库自带的一个经典的刷新和上拉加载样式
        refreshLayout.addHeaderComponent(new ClassicHeader(this));
        refreshLayout.addFooterComponent(new ClassicFooter(this));

```

3. 开发者如何自定义自己的刷新和上拉样式
```java
public class MyRefreshHeader extends AbstractRefreshHeader {

    public MyRefreshHeader(Context context) {
        super(context);
    }

    @Override
    public void onPullRefreshRate(int progress) {
        // 此处是下拉的进度 0-100，该方法主要用于下拉时候的动效监听，
        // 开发者可根据progress的值来不断刷新样式，从而达到一个下拉的酷炫效果
    }

    @Override
    public int getHeaderLayoutId() {
        // 此处直接返回开发自己编写的刷新头布局文件id即可
        return 0;
    }

    @Override
    public void onPullRefreshStateChange(RefreshState refreshState) {
        // 此处是下拉刷新的状态变化，总共有三种状态
        // STATE_NORMAL 正常状态
        // STATE_READY_REFRESH 准备刷新状态，即表示用户已经下拉达到了头布局的最大刷新高度，释放就可以刷新的一种状态
        // STATE_REFRESHING 正在刷新状态
        
    }
}
```

```java
public class MyRefreshFooter extends AbstractRefreshFooter {

    public MyRefreshFooter(Context context) {
        super(context);
    }

    @Override
    public void onPullLoadMoreRate(int progress) {
        // 此处是上拉的进度 0-100，该方法主要用于上拉时候的动效监听，
        // 开发者可根据progress的值来不断底部样式，从而达到一个上拉的酷炫效果
    }

    @Override
    public int getFooterLayoutId() {
        // 此处直接返回开发自己编写的上拉底部布局文件id即可
        return 0;
    }

    @Override
    public void onPullLoadMoreStateChange(LoadMoreState loadMoreState) {
        // 此处是上拉加载更多的状态变化，总共有三种状态
        // STATE_NORMAL 正常状态
        // STATE_READY_LOADMORE 准备加载更多状态，即表示用户已经上拉达到了底部布局的最大高度，释放就可以加载更多的一种状态
        // STATE_LOADING 正在加载状态
    }
}
```

4. 特性介绍
* 下拉刷新是默认开启的，上拉加载更多是默认关闭的
* 支持自动刷新，延迟停止刷新和延迟停止加载更多
* 支持自定义的头部和底部样式
* 支持对任何组件进行下拉刷新和上拉加载
* 该组件耦合性低

5. 如有更好建议或需求，请直接提issue或发送邮件(gaojianming108@126.com)

### 版权和许可信息
```
Apache License 2.0
```


